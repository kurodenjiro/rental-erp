frappe.ui.form.on('Payment Entry', {
    // setup:function(){
    //     frm.set_query("equipment_name", function() {
	// 		return{
	// 			"filters": {
	// 				"sales_invoice":cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name
	// 			}
	// 		}
	// 	});
    // },
    equipment_name:function(frm){
        // console.log(cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_type)
        // console.log(cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name)
        
        if(!frm.doc.mode_of_payment){
            frappe.msgprint("Please select <b>Mode Of Payment</b> first")
            frm.set_value("equipment_name","")
            frm.refresh_field("equipment_name")
        }
        if(frm.doc.equipment_name){
            if(!cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name){
                frappe.throw("Please Enter <b>Reference Sales Invoice</b> First")
            }
            frappe.call({
                method: "rental.rental_operation.payment_entry.get_equipment_outstanding",
                args: {
                    "sale_invoice":cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name,
                    "equipment_name":frm.doc.equipment_name},
                callback: function(r) {

                    console.log(r.message)
                    if(r.message[0]['name'] == null){

                        frappe.call({
                            method: "rental.rental_operation.payment_entry.get_total",
                            args: {
                                "sale_invoice":cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name,
                                "equipment_name":frm.doc.equipment_name},
                                callback: function(r) {
                                console.log(r.message)
                                var total_outstanding = 0
                                frm.set_value("equipment_wise_outstanding_amount",[])
                                
                                for (let i = 0; i < r.message.length; i++) {
                                    const element = r.message[i];
                                    var total_amount = 0
                                    if(element['total_taxes_and_charges']){
                                        total_amount = element['amount'] + (element['total_taxes_and_charges'] * element['amount'])/element['total']
                                    }else{
                                        total_amount = element['amount']
                                    }
                                   
                                    var newrow = frappe.model.add_child(frm.doc, "Equipment Wise Outstanding Amount", "equipment_wise_outstanding_amount");
                                    newrow.equipment_name = frm.doc.equipment_name;
                                    newrow.total_amount = total_amount;
                                    newrow.paid_amount = 0;
                                    newrow.outstanding_amount = total_amount;
                                    total_outstanding = total_outstanding + total_amount;
                                    refresh_field("equipment_wise_outstanding_amount");
                                    
                                }
                                frm.set_value("paid_amount",total_outstanding)
                                frm.refresh_field("paid_amount")
                                frm.set_df_property("equipment_wise_outstanding_amount","read_only",1);
                            }
                        })

                    }else{
                        frm.set_value("equipment_wise_outstanding_amount",[])
                        var total_outstanding = 0
                        for (let i = 0; i < r.message.length; i++)  {
                            const element = r.message[i];
                            var total_amount = 0
                            if(element['total_taxes_and_charges']){
                                total_amount = element['amount'] + (element['total_taxes_and_charges'] * element['amount'])/element['total']
                            }else{
                                total_amount = element['amount']
                            }

                            var newrow = frappe.model.add_child(frm.doc, "Equipment Wise Outstanding Amount", "equipment_wise_outstanding_amount");
                            var outstanding_amount = total_amount - element['paid_amount']
                            newrow.equipment_name = frm.doc.equipment_name;
                            newrow.total_amount = total_amount;
                            newrow.paid_amount = element['paid_amount'];
                            newrow.outstanding_amount = outstanding_amount
                            total_outstanding = total_outstanding + outstanding_amount
                            refresh_field("equipment_wise_outstanding_amount");
                            
                        }
                        frm.set_value("paid_amount",total_outstanding)
                        frm.refresh_field("paid_amount")
                        frm.set_df_property("equipment_wise_outstanding_amount","read_only",1);
                    }
                    
                }
            });
        }
        

    },

    setup:function(frm){

        cur_frm.set_query("equipment_name",function(){
            console.log(cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name)
            return{
                filters:{
                    "sales_invoice":cur_frm.fields_dict['references'].grid.grid_rows[0].doc.reference_name,
                },
                query: "rental.rental_operation.payment_entry.get_invoice_equipment"
            }
        })

    }
    
})

cur_frm.fields_dict['equipment_wise_outstanding_amount'].grid.get_field('equipment_name').get_query = function(doc) {
	return {
		filters:{ 
			"is_repairing_item":1 
		}
	}
}