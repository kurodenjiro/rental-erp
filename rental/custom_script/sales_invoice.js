frappe.ui.form.on('Sales Invoice', {
    refresh:function(frm){

    },
    onload:function(frm){
        cur_frm.set_df_property("get_renting_price","reqd",1)
        
        if(frm.doc.renting_type == "Daily Equipment Renting"){
            frm.set_value("daily_equipment_renting",frm.doc.rent_reference)
            frm.refresh_field("daily_equipment_renting")
        }
    }
})

frappe.ui.form.on('Sales Invoice Item', {
    rented_day:function(frm,cdt,cdn){
        var d = locals[cdt][cdn]
        if(d.renting_type == "Daily"){
            if(d.rented_day && d.daily_rate){
                d.amount = d.rented_day * d.daily_rate
                d.rate = d.rented_day * d.daily_rate
            }
            
        }
        frm.refresh_field("items")
    },
    daily_rate:function(frm,cdt,cdn){
        console.log("here")
        var d = locals[cdt][cdn]
        cur_frm.script_manager.trigger("rented_day", d.doctype, d.name);

    },
    rented_hours:function(frm,cdt,cdn){
        var d = locals[cdt][cdn]
        if(d.renting_type == "Hourly"){
            if(d.rented_hours && d.hourly_rate){
            d.amount = d.rented_hours * d.hourly_rate
            d.rate = d.rented_hours * d.hourly_rate
            }
        }
        frm.refresh_field("items")
    },
    hourly_rate:function(frm,cdt,cdn){
        var d = locals[cdt][cdn]
        cur_frm.script_manager.trigger("rented_hours", d.doctype, d.name);

    }

})

cur_frm.cscript.get_renting_price = function(doc,cdt,cdn){
    // console.log("here")
    if(doc.get_renting_price == 1){
        var value = locals[cdt][cdn]
        var df = frappe.meta.get_docfield("Sales Invoice Item","renting_rate", doc.name);
        value.items.forEach(element => {
        element.rate = element.renting_rate
    });
    }
    
}