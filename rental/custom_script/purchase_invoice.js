frappe.ui.form.on('Purchase Invoice', {
    onload: function(frm) {
        frm.remove_custom_button("Payment", 'Create');
    },
    refresh:function(frm){
        

        if(frm.doc.docstatus == 1 && frm.doc.status != "Paid"){
            frm.remove_custom_button("Payment", 'Create');
		// 	cur_frm.add_custom_button(__('Payment Via JV'), function() {
		// 		frappe.model.open_mapped_doc({
		// 		method: "rental.rental_operation.purchase_invoice.get_map_doc",
		// 		frm: cur_frm
		// 		})
		// 	}, __("Create"));
        // }
        cur_frm.add_custom_button(__('Payment Via JV'), function() {
            // console.log("in main function")
            return frappe.call({
                method:"rental.rental_operation.purchase_invoice.get_payment_entry_against_invoice",
                args: {
                    "dt": cur_frm.doc.doctype,
                    "dn": cur_frm.doc.name
                },
                callback: function(r) {
                    var doclist = frappe.model.sync(r.message);
                    frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
                    // cur_frm.refresh_fields()
                }
            });
        }, __("Create"));
    }

    frm.set_query("equipment",function(){
        return{
            filters:{
                "is_rental_equipment":1
            }
        }
    })
    }
    // get_method_for_payment: function(){
    //     var method = "erpnext.accounts.doctype.journal_entry.journal_entry.get_payment_entry_against_invoice";	
    //     re
	// }
    // is_equipment_invoice:function(frm){
    //     if(frm.doc.is_equipment_invoice){
    //         cur_frm.set_df_property("equipment","reqd",1)
    //     }else{
    //         cur_frm.set_df_property("equipment","reqd",0)
    //     }
    // }

})