frappe.ui.form.on('Item', {
    setup:function(frm){
        
        // Display Only financial partner
        frm.set_query("partner_name", "equipment_partners", function() {
			return{
                filters:{
                    "is_financial_partner":1
                }
            }
		});
    },
    partner_group:function(frm){
        // console.log(frm.doc.partner_group)
        frappe.call({
            method:"rental.rental_operation.item.sales_partner",
            args:{
                "group":frm.doc.partner_group
            },
            callback:function(r){
                console.log(r.message) 
                frm.set_value("equipment_partners",[])
                for (let i = 0; i < r.message.length; i++) {
                    const element = r.message[i];
                    var newrow = frappe.model.add_child(frm.doc, "Equipment Partners", "equipment_partners");
                    newrow.partner_name = element['partner_name'];   
                    refresh_field("equipment_partners");
                    cur_frm.script_manager.trigger("partner_name", newrow.doctype, newrow.name);
                                    
                }               
            }
        })
    },
    is_rental_equipment:function(frm){
        if(frm.doc.is_rental_equipment == 1){
            cur_frm.set_df_property("partner_group","reqd",1)
        }else{
            cur_frm.set_df_property("partner_group","reqd",0)
        }
    }

})