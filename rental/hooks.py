# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "rental"
app_title = "Rental Operation"
app_publisher = "Raj Tailor"
app_description = "It Used to keep track of rented equipment"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "tailorraj111@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/rental/css/rental.css"
# app_include_js = "/assets/rental/js/rental.js"

# include js, css files in header of web template
# web_include_css = "/assets/rental/css/rental.css"
# web_include_js = "/assets/rental/js/rental.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}

doctype_js = {
	"Item" : "custom_script/item.js",
	"Employee" : "custom_script/employee.js",
	"Sales Invoice" : "custom_script/sales_invoice.js",
	"Purchase Invoice" : "custom_script/purchase_invoice.js",
	"Payment Entry" : "custom_script/payment_entry.js"
}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "rental.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "rental.install.before_install"
# after_install = "rental.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "rental.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Journal Entry":{
		"validate":"rental.rental_operation.journal_entry.validate"
	},
	"Sales Partner":{
		"validate":"rental.rental_operation.sales_partner.validate"
		# "after_insert":"rental.rental_operation.sales_partner.after_insert"
	},
	"Item":{
		"validate":"rental.rental_operation.item.validate",
		"after_insert":"rental.rental_operation.item.after_insert"
	},
	"Sales Invoice":{
		"validate":"rental.rental_operation.sales_invoice.validate",
		"on_submit":"rental.rental_operation.sales_invoice.on_submit",
		"after_insert":"rental.rental_operation.sales_invoice.after_insert",
		"on_cancel":"rental.rental_operation.sales_invoice.on_cancel",
		"before_cancel":"rental.rental_operation.sales_invoice.before_cancel"
	},
	"Purchase Invoice":{
		"validate":"rental.rental_operation.purchase_invoice.validate",
		"on_submit":"rental.rental_operation.purchase_invoice.on_submit",
		"on_cancel":"rental.rental_operation.purchase_invoice.on_cancel"
	},
	"Payment Entry":{
		"validate":"rental.rental_operation.payment_entry.validate",
		"on_submit":"rental.rental_operation.payment_entry.on_submit",
		"on_cancel":"rental.rental_operation.payment_entry.on_cancel"
	}
}

fixtures = ["Workflow","Custom Field","Property Setter"]

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"rental.tasks.all"
# 	],
# 	"daily": [
# 		"rental.tasks.daily"
# 	],
# 	"hourly": [
# 		"rental.tasks.hourly"
# 	],
# 	"weekly": [
# 		"rental.tasks.weekly"
# 	]
# 	"monthly": [
# 		"rental.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "rental.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "rental.event.get_events"
# }

