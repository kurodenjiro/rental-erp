# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	config = [
		{
			"label": _("Masters"),
			"items": [
				{
					"type": "doctype",
					"name": "Item",
					"description": _("Enter Renting Equipment and Repairing Equipment here"),
					"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "Employee",
					"description": _("Enter Information Of Technician and Handling Employee"),
					"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "Sales Partner",
					"description": _("Enter Individual Sales Partner here"),
					"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "Sales Partner Group",
					"description": _("Enter Sales Owner here which will have different sales partners"),
					"onboard": 1,
				}
			]
		},
		{
			"label": _("Timesheets"),
			"items": [
				{
					"type": "doctype",
					"name": "Equipment Renting Timesheet",
					"description": _("Make Entry of rented equipment."),
					"onboard": 1,
				},
				{
					"type": "doctype",
					"name": "Daily Equipment Renting",
					"description": _("Maintain Daily Equipment Renting Here."),
					"onboard": 1,
				}
			]
		},
		{
			"label":_("Equipment Maintenance"),
			"items":[
				{
					"type": "doctype",
					"name": "Equipment Maintenance",
					"description": _("Manage Maintenance of Equipment here"),
					"onboard": 1,
				}
			]
		},
		{
			"label":_("Sales"),
			"items":[
				{
					"type": "doctype",
					"name": "Sales Invoice",
					"description": _("All Your Invoices Maintained here."),
					"onboard": 1,
				}
			]
		},
		{
			"label":_("Purchase"),
			"items":[
				{
					"type": "doctype",
					"name": "Purchase Invoice",
					"description": _("All Your Purchase Invoices Maintained here."),
					"onboard": 1,
				}
			]
		},
		{
			"label":_("Standard Reports"),
			"items":[
				{
					"type": "report",
					"name": "General Ledger",
					"doctype": "GL Entry",
					"is_query_report": True,
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Stock Ledger",
					"doctype": "Stock Ledger Entry",
					"onboard": 1,
					"dependencies": ["Item"],
				}
			]
		},
		{
			"label":_("Rental Operation Reports"),
			"items":[
				{
					"type": "report",
					"name": "Equipment Earning and Expenditure",
					"doctype": "Equipment Renting Timesheet",
					"is_query_report": True,
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Sales Owner Wise Outstanding",
					"doctype": "Sales Invoice",
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Equipment Wise Sales",
					"doctype": "Sales Invoice",
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Sales Person Wise Sales",
					"doctype": "Sales Invoice",
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Sales Summary",
					"doctype": "Sales Invoice",
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Sales Owner Wise Earning and Expense",
					"doctype": "Sales Invoice",
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Supplier Wise Purchase and Outstanding",
					"doctype": "Sales Invoice",
					"onboard":1
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Statement of Account",
					"doctype": "Sales Invoice",
					"onboard":1
				}
				
			]
		}
	]

	return config

