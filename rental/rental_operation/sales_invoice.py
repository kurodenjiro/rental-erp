import frappe
from frappe.utils import flt, cint, cstr, add_days, getdate
from erpnext.accounts.utils import get_fiscal_year, validate_fiscal_year, get_account_currency
from erpnext.controllers.accounts_controller import AccountsController as ac
# from erpnext.accounts.general_ledger import make_gl_entries
import json


def validate(self,method):
    if self.renting_type == "Equipment Renting Timesheet":
        if self.get_renting_price == 0:
            frappe.throw("Please Check <b>Get Renting Rate</b>")

def after_insert(self,method):
    if self.rent_reference and self.renting_type == "Equipment Renting Timesheet":
        frappe.db.set_value("Equipment Renting Timesheet",str(self.rent_reference),"timesheet_invoice",self.name)

def on_submit(self,method):
    if self.renting_type:
        make_gl_entries_custom(self)

def before_cancel(self,method):
    # frappe.msgprint("before here")
    frappe.db.set_value("Equipment Renting Timesheet",str(self.rent_reference),"timesheet_invoice","")
    self.rent_reference = None
    frappe.db.set_value("Sales Invoice",str(self.name),"rent_reference","")

def on_cancel(self,method):
    
    self.rent_reference = None
    frappe.db.set_value("Sales Invoice",str(self.name),"rent_reference","")
    if self.renting_type:
        make_gl_entries_custom(self)
    


def make_gl_entries_custom(self,gl_entries=None):

    if not gl_entries:
        gl_entries = get_gl_entries(self)

    # frappe.throw(cstr(gl_entries))
    if gl_entries:
        from erpnext.accounts.general_ledger import make_gl_entries
        make_gl_entries(gl_entries, cancel=(self.docstatus == 2),
                        update_outstanding="No", merge_entries=False)


def get_gl_entries(self):
    gl_entries = []
    # payment_account_currency = get_account_currency(self.debit_to)
    # sales_partner_acc = self.sales_partner_account
        
    # if float(self.total) > 0:
    #     account_currency = get_account_currency(str(sales_partner_acc))


    for item in self.items:
        payment_account_currency = get_account_currency(self.debit_to)
        sales_partner_acc = str(item.sales_partner_account)
            
        if float(self.total) > 0:
            account_currency = get_account_currency(str(sales_partner_acc))
        gl_entries.append(
            self.get_gl_dict({
                "posting_date": self.posting_date,
                "account": self.debit_to,
                "party_type":"Customer",
                "party":self.customer,
                "against": str(item.sales_partner_account),
                "credit": flt(item.amount),
                "credit_in_account_currency": flt(item.amount),
                "remarks": "Againts Invoice {}".format(self.name)
            }, payment_account_currency)
        )

        gl_entries.append(
            self.get_gl_dict({
                "posting_date": self.posting_date,
                "account": str(item.sales_partner_account),
                "against": self.debit_to,
                "debit": flt(item.amount),
                "party_type":"Customer",
                "party":self.customer,
                "debit_in_account_currency": flt(item.amount),
                "against_voucher": self.name,
                "against_voucher_type": self.doctype,
                "cost_center": self.cost_center,
                "remarks": "Againts Invoice {}".format(self.name)
            }, account_currency)
        )

    return gl_entries



# def get_partners(self):
#     partner_data = frappe.db.get_values("Equipment Partners",
#         fieldname=["partner_name", "partner_distribution","partner_account"],
#         filters={
#             "parent": self.time_sheet_reference
#         },
#         as_dict=True
#     )

#     partner_share = []
#     for data in partner_data:
#         share = (data.partner_distribution * self.rounded_total)/100
#         p_dict = {
#             'partner':str(data.partner_name),
#             'share_amount':float(share),
#             'account':str(data.partner_account)
#         }
#         partner_share.append(p_dict)
    
#     return partner_share
