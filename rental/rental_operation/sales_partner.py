import frappe

def validate(doc,method):
    update_sales_partner_group(doc)
    # frappe.msgprint("here")

def after_insert(doc,method):
    pass
    # if doc.is_financial_partner == 1:
    #     object = frappe.get_doc({
    #     "doctype":"Account",
    #     "account_name":doc.partner_name,
    #     "parent_account":"Sales Partner - AM",
    #     "company":"Akhilam"
    #     })
    #     object.flags.ignore_permissions = True
    #     object.insert()
    #     doc.partner_account = object.name
    #     frappe.db.set_value("Sales Partner",doc.name,"partner_account",object.name)

        # frappe.msgprint("here")

def update_sales_partner_group(doc):
    if doc.sales_partner_group:
        sales_partner = {
            "partner_name":doc.name
        }
        parent = frappe.get_doc("Sales Partner Group",doc.sales_partner_group)
        parent.append("sales_partners",sales_partner)
        parent.flags.ignore_permissions = True
        parent.save()
        frappe.db.commit()
