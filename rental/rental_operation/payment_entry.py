import frappe
from frappe.utils import flt, cint, cstr, add_days, getdate
from erpnext.accounts.utils import get_fiscal_year, validate_fiscal_year, get_account_currency
from erpnext.controllers.accounts_controller import AccountsController as ac


def validate(self,method):
    if self.get_payment_against_equipment:
        for item in self.equipment_wise_outstanding_amount:
            if item.outstanding_amount == 0:
                frappe.throw("Payment For this equiment is already made")

def on_submit(self,method):
    if self.get_payment_against_equipment:
        make_gl_entries_custom(self)

def on_cancel(self,method):
    if self.get_payment_against_equipment:
        make_gl_entries_custom(self)

def make_gl_entries_custom(self,gl_entries=None):

    if not gl_entries:
        gl_entries = get_gl_entries(self)

    # frappe.throw(cstr(gl_entries))
    if gl_entries:
        from erpnext.accounts.general_ledger import make_gl_entries
        make_gl_entries(gl_entries, cancel=(self.docstatus == 2),
                        update_outstanding="No", merge_entries=False)

def get_gl_entries(self):
    gl_entries = []
    # payment_account_currency = get_account_currency(self.debit_to)
    # sales_partner_acc = self.sales_partner_account
        
    # if float(self.total) > 0:
    #     account_currency = get_account_currency(str(sales_partner_acc))

    payment_account_currency = get_account_currency(self.sales_partner_account)
        
    if float(self.paid_amount) > 0:
        account_currency = get_account_currency(str(self.paid_from))
    gl_entries.append(
        self.get_gl_dict({
            "posting_date": self.posting_date,
            "account": self.sales_partner_account,
            "party_type":"Customer",
            "party":self.party,
            "against": str(self.paid_from),
            "credit": flt(self.paid_amount),
            "credit_in_account_currency": flt(self.paid_amount),
            "remarks": "Againts Payment Entry {}".format(self.name)
        }, payment_account_currency)
    )

    gl_entries.append(
        self.get_gl_dict({
            "posting_date": self.posting_date,
            "account": str(self.paid_from),
            "against": self.sales_partner_account,
            "debit": flt(self.paid_amount),
            "party_type":"Customer",
            "party":self.party,
            "debit_in_account_currency": flt(self.paid_amount),
            "against_voucher": self.name,
            "against_voucher_type": self.doctype,
            "remarks": "Againts Payment Entry {}".format(self.name)
        }, account_currency)
    )

    return gl_entries

@frappe.whitelist()
def get_equipment_outstanding(sale_invoice,equipment_name):
    # equipment_payment  = frappe.db.sql(""" select si.amount,pe.total_allocated_amount,(si.amount - pe.total_allocated_amount) as outstanding,pe.name,si.parent from `tabSales Invoice Item` as si inner join `tabPayment Entry` as pe on si.item_code = pe.equipment_name inner join `tabPayment Entry Reference` pf on si.parent = pf.reference_name""",as_dict = 1)

    equipment_payment = frappe.db.sql("""select si.amount,sum(pe.paid_amount) as paid_amount,pe.name,si.parent,s.name as sales_invoice,s.total,s.total_taxes_and_charges from  `tabSales Invoice Item` as si inner join `tabPayment Entry Reference` as pf on si.parent = pf.reference_name inner join `tabPayment Entry` as pe on pf.parent = pe.name inner join `tabSales Invoice` as s on s.name = si.parent where si.parent = %s and pe.equipment_name = %s and si.item_code = %s and pe.docstatus = 1""",(sale_invoice,equipment_name,equipment_name),as_dict = 1)

    # equipment_payment  = frappe.db.sql(""" select si.amount,pe.total_allocated_amount,(si.amount - pe.total_allocated_amount),pe.name as outstanding from `tabSales Invoice Item` as si inner join `tabPayment Entry` as pe on si.item_code = pe.equipment_name where si.parent = %s and si.item_code = %s and pe.docstatus = 1""",(str(sale_invoice),str(equipment_name)),as_dict = 1)
    # frappe.msgprint(str(equipment_payment))
    return equipment_payment

@frappe.whitelist()
def get_total(sale_invoice,equipment_name):
    equipment_payment  = frappe.db.sql(""" select si.amount , s.total, s.total_taxes_and_charges from `tabSales Invoice Item` as si inner join `tabSales Invoice` as s on s.name = si.parent where s.name = %s and si.item_code = %s""",(str(sale_invoice),str(equipment_name)),as_dict = 1)
    return equipment_payment

def get_invoice_equipment(doctype, txt, searchfield, start, page_len, filters):
    return frappe.db.sql("""select item_code from `tabSales Invoice Item` as si where si.parent = %s""",filters.get("sales_invoice"))
    


    
    