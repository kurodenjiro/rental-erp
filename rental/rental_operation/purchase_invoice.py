import frappe,erpnext
from frappe import _
from erpnext.accounts.utils import get_fiscal_year, validate_fiscal_year, get_account_currency
from erpnext.accounts.doctype.invoice_discounting.invoice_discounting import get_party_account_based_on_invoice_discounting
from erpnext.accounts.utils import get_balance_on, get_account_currency

def validate(self,method):
	if self.is_equipment_invoice:
		if self.update_stock == 1:
			frappe.throw("Please Uncheck <b>Update Stock</b> For Equpment Invoice")
    #     for item in self.items:
    #         item.expense_account = self.equipment_cost_account
        # self.credit_to = self.partner_group_account_
        # frappe.msgprint("set")

def on_submit(self,method):
    pass
    # if self.is_equipment_invoice:
    #     make_gl_entries_custom(self)

def on_cancel(self,method):
    pass
    # if self.is_equipment_invoice:
    #     make_gl_entries_custom(self)




def make_gl_entries_custom(self,gl_entries=None):

    if not gl_entries:
        gl_entries = get_gl_entries(self)

    # frappe.throw(cstr(gl_entries))
    if gl_entries:
        from erpnext.accounts.general_ledger import make_gl_entries
        make_gl_entries(gl_entries, cancel=(self.docstatus == 2),
                        update_outstanding="No", merge_entries=False)


def get_gl_entries(self):
    gl_entries = []
    payment_account_currency = get_account_currency(self.partner_group_account_)
    credit_acc = self.credit_to
        
    if float(self.total) > 0:
        account_currency = get_account_currency(str(credit_acc))

        gl_entries.append(
            self.get_gl_dict({
                "posting_date": self.posting_date,
                "account": self.partner_group_account_,
                "party_type":"Supplier",
                "party":self.supplier,
                "against": str(credit_acc),
                "credit": float(self.total),
                "credit_in_account_currency": float(self.total),
                "remarks": "Againts Invoice {}".format(self.name)
            }, payment_account_currency)
        )

        gl_entries.append(
            self.get_gl_dict({
                "posting_date": self.posting_date,
                "account": str(credit_acc),
                "against": self.partner_group_account_,
                "debit": float(self.total),
                "party_type":"Supplier",
                "party":self.supplier,
                "debit_in_account_currency": float(self.total),
                "against_voucher": self.name,
                "against_voucher_type": self.doctype,
                "cost_center": self.cost_center,
                "remarks": "Againts Invoice {}".format(self.name)
            }, account_currency)
        )

    return gl_entries

def get_exchange_rate(posting_date, account=None, account_currency=None, company=None,
		reference_type=None, reference_name=None, debit=None, credit=None, exchange_rate=None):
	from erpnext.setup.utils import get_exchange_rate
	account_details = frappe.db.get_value("Account", account,
		["account_type", "root_type", "account_currency", "company"], as_dict=1)

	if not account_details:
		frappe.throw(_("Please select correct account"))

	if not company:
		company = account_details.company

	if not account_currency:
		account_currency = account_details.account_currency

	company_currency = erpnext.get_company_currency(company)

	if account_currency != company_currency:
		if reference_type in ("Sales Invoice", "Purchase Invoice") and reference_name:
			exchange_rate = frappe.db.get_value(reference_type, reference_name, "conversion_rate")

		# The date used to retreive the exchange rate here is the date passed
		# in as an argument to this function.
		elif (not exchange_rate or exchange_rate==1) and account_currency and posting_date:
			exchange_rate = get_exchange_rate(account_currency, company_currency, posting_date)
	else:
		exchange_rate = 1

	# don't return None or 0 as it is multipled with a value and that value could be lost
	return exchange_rate or 1

@frappe.whitelist()
def get_default_bank_cash_account(company, account_type=None, mode_of_payment=None, account=None):
	from erpnext.accounts.doctype.sales_invoice.sales_invoice import get_bank_cash_account
	if mode_of_payment:
		account = get_bank_cash_account(mode_of_payment, company).get("account")

	if not account:
		'''
			Set the default account first. If the user hasn't set any default account then, he doesn't
			want us to set any random account. In this case set the account only if there is single
			account (of that type), otherwise return empty dict.
		'''
		if account_type=="Bank":
			account = frappe.get_cached_value('Company',  company,  "default_bank_account")
			if not account:
				account_list = frappe.get_all("Account", filters = {"company": company,
					"account_type": "Bank", "is_group": 0})
				if len(account_list) == 1:
					account = account_list[0].name

		elif account_type=="Cash":
			account = frappe.get_cached_value('Company',  company,  "default_cash_account")
			if not account:
				account_list = frappe.get_all("Account", filters = {"company": company,
					"account_type": "Cash", "is_group": 0})
				if len(account_list) == 1:
					account = account_list[0].name

	if account:
		account_details = frappe.db.get_value("Account", account,
			["account_currency", "account_type"], as_dict=1)

		return frappe._dict({
			"account": account,
			"balance": get_balance_on(account),
			"account_currency": account_details.account_currency,
			"account_type": account_details.account_type
		})
	else: return frappe._dict()

def get_payment_entry(ref_doc, args):
	cost_center = ref_doc.get("cost_center") or frappe.get_cached_value('Company',  ref_doc.company,  "cost_center")
	exchange_rate = 1
	if args.get("party_account"):
		# Modified to include the posting date for which the exchange rate is required.
		# Assumed to be the posting date in the reference document
		exchange_rate = get_exchange_rate(ref_doc.get("posting_date") or ref_doc.get("transaction_date"),
			args.get("party_account"), args.get("party_account_currency"),
			ref_doc.company, ref_doc.doctype, ref_doc.name)

	je = frappe.new_doc("Journal Entry")
	je.update({
		"voucher_type": "Bank Entry",
		"company": ref_doc.company,
		"remark": args.get("remarks")
	})

	party_row = je.append("accounts", {
		"account": args.get("party_account"),
		"party_type": args.get("party_type"),
		"party": ref_doc.get(args.get("party_type").lower()),
		"cost_center": cost_center,
		"account_type": frappe.db.get_value("Account", args.get("party_account"), "account_type"),
		"account_currency": args.get("party_account_currency") or \
							get_account_currency(args.get("party_account")),
		"balance": get_balance_on(args.get("party_account")),
		"party_balance": get_balance_on(party=args.get("party"), party_type=args.get("party_type")),
		"exchange_rate": exchange_rate,
		args.get("amount_field_party"): args.get("amount"),
		"is_advance": args.get("is_advance"),
		"reference_type": ref_doc.doctype,
		"reference_name": ref_doc.name
	})

	bank_row = je.append("accounts")

	# Make it bank_details
	bank_account = get_default_bank_cash_account(ref_doc.company, "Bank", account=args.get("bank_account"))
	if bank_account:
		bank_row.update(bank_account)
		# Modified to include the posting date for which the exchange rate is required.
		# Assumed to be the posting date of the reference date
		bank_row.exchange_rate = get_exchange_rate(ref_doc.get("posting_date")
			or ref_doc.get("transaction_date"), bank_account["account"],
			bank_account["account_currency"], ref_doc.company)

	bank_row.cost_center = cost_center

	amount = args.get("debit_in_account_currency") or args.get("amount")

	if bank_row.account_currency == args.get("party_account_currency"):
		bank_row.set(args.get("amount_field_bank"), amount)
	else:
		bank_row.set(args.get("amount_field_bank"), amount * exchange_rate)

	# Multi currency check again
	if party_row.account_currency != ref_doc.company_currency \
		or (bank_row.account_currency and bank_row.account_currency != ref_doc.company_currency):
			je.multi_currency = 1

	je.set_amounts_in_company_currency()
	je.set_total_debit_credit()

	return je if args.get("journal_entry") else je.as_dict()






@frappe.whitelist()
def get_payment_entry_against_invoice(dt, dn, amount=None,  debit_in_account_currency=None, journal_entry=False, bank_account=None):
	ref_doc = frappe.get_doc(dt, dn)
	if dt == "Sales Invoice":
		party_type = "Customer"
		party_account = get_party_account_based_on_invoice_discounting(dn) or ref_doc.debit_to
	else:
		party_type = "Supplier"
		party_account = ref_doc.credit_to

	if (dt == "Sales Invoice" and ref_doc.outstanding_amount > 0) \
		or (dt == "Purchase Invoice" and ref_doc.outstanding_amount < 0):
			amount_field_party = "credit_in_account_currency"
			amount_field_bank = "debit_in_account_currency"
	else:
		amount_field_party = "debit_in_account_currency"
		amount_field_bank = "credit_in_account_currency"

	return get_payment_entry(ref_doc, {
		"party_type": party_type,
		"party_account": party_account,
		"party_account_currency": ref_doc.party_account_currency,
		"amount_field_party": amount_field_party,
		"amount_field_bank": amount_field_bank,
		"amount": amount if amount else abs(ref_doc.outstanding_amount),
		"debit_in_account_currency": debit_in_account_currency,
		"remarks": '{0}'.format(dn),
		"is_advance": "No",
		"bank_account": bank_account,
		"journal_entry": journal_entry
	})

