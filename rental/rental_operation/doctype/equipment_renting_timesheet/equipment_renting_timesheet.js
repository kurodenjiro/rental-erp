// Copyright (c) 2019, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Equipment Renting Timesheet', {
	onload:function(frm){
		// console.log(frappe.defaults.get_user_default("Company"))
		frm.set_value("company",frappe.defaults.get_user_default("company"))
		frm.refresh_field("company")
	},
	refresh:function(frm){
		if(frm.doc.docstatus == 1 && !frm.doc.timesheet_invoice){
			cur_frm.add_custom_button(__('Sales Invoice'), function() {
				frappe.model.open_mapped_doc({
				method: "rental.rental_operation.doctype.equipment_renting_timesheet.equipment_renting_timesheet.get_map_doc",
				frm: cur_frm
				})
			}, __("Make"));
		}
	},


	setup:function(frm){
		frm.set_query("equipment_name",function(){
			return{
				filters:{
					"is_rental_equipment":1
				}
				
			}
		})
	},
	equipment_name:function(frm){
		frm.set_value("partners",[])
		if(frm.doc.equipment_name){
			frappe.call({
				method: "rental.rental_operation.doctype.equipment_renting_timesheet.equipment_renting_timesheet.get_partners",
				args: {"item":frm.doc.equipment_name},
				callback: function(r) {
					console.log(r.message)
					for (let i = 0; i < r.message.length; i++) {
						const element = r.message[i];
						console.log(element)
						var newrow = frappe.model.add_child(frm.doc, "Equipment Partners", "partners");
						newrow.partner_name = element['partner_name'];
						newrow.partner_distribution = element['partner_distribution'];
						refresh_field("partners");
						cur_frm.script_manager.trigger("partners", newrow.doctype, newrow.name);
					}
				}
			});
		}else{
			frm.set_value("partners",[])
			refresh_field("partners");
		}
	}

	
});

frappe.ui.form.on('Multiple Equipment Rent', {

	actual_hours:function(frm,cdt,cdn){
		console.log("here")
		var df = frappe.meta.get_docfield("Multiple Equipment Rent","reason_to_change_hours", frm.doc.name);
		console.log(df)
		var d = locals[cdt][cdn]
		// if(d.actual_hours){
		// 	df.reqd = 1
		// 	frm.refresh_field("renting_details")
        // }else{
		// 	df.reqd = 0
		// 	frm.refresh_field("renting_details")
		// }

		d.total_amount = d.actual_hours * d.hourly_rate
		frm.refresh_field("renting_details")

	},
	hourly_rate:function(frm,cdt,cdn){
		var d = locals[cdt][cdn]
		cur_frm.script_manager.trigger("actual_hours", d.doctype, d.name);
	},

	from_date:function(frm,cdt,cdn){
		var d = locals[cdt][cdn]
		if(d.from_date && d.to_date){
			var rented_day = frappe.datetime.get_day_diff(d.to_date,d.from_date )
			if(rented_day >= 0 ){
				d.rented_day = rented_day + 1
				// console.log(d.rented_day)
				// console.log(d.rate)
				if(d.daily_rate){
					d.total_amount = d.rented_day * d.daily_rate
				}
				
				frm.refresh_field("renting_details")
				// console.log(rented_day)
			}
			else{
				d.rented_day = ""
				d.total_amount = ""
				frm.refresh_field("renting_details")
				frappe.throw("<b>From Date</b> Should be Greater Than <b>to Date</b>")
			}
		}


	},

	to_date:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		cur_frm.script_manager.trigger("from_date", row.doctype, row.name);
	},

	daily_rate:function(frm,cdt,cdn){
		var d = locals[cdt][cdn]
		if(d.rented_day && d.daily_rate){
			d.total_amount = d.rented_day * d.daily_rate
			frm.refresh_field("renting_details")
		}
		
	}

})

cur_frm.fields_dict['renting_details'].grid.get_field('equipment_name').get_query = function(doc) {
	return {
		filters:{ 
			"is_rental_equipment":1 
		}
	}
}

cur_frm.fields_dict['renting_details'].grid.get_field('handling_employee').get_query = function(doc) {
	return {
		filters:{ 
			"is_equipment_handler":1 
		}
	}
}