# -*- coding: utf-8 -*-
# Copyright (c) 2019, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import date_diff,getdate



class EquipmentRentingTimesheet(Document):
	def on_update_after_submit(self):
		pass
		# frappe.throw("here")
		# for details in self.renting_details:
		# 	if (details.actual_hours and details.hourly_rate and not details.reason_to_change_hours):
		# 		frappe.throw("Please Mention <b>Reason For Changing Hours</b> in <b>Row {}</b>".format(details.idx))

		self.calculate_total_cost()

	def on_submit(self):
		self.update_equipment()
		for equipment in self.renting_details:
			self.create_booking(equipment.equipment_name,equipment.from_date,equipment.to_date)

	def on_cancel(self):
		for equipment in self.renting_details:
			frappe.delete_doc("Equipment Booking",frappe.db.get_value("Equipment Booking",{"timesheet":self.name,"equipment":equipment.equipment_name},"name"))

	def validate(self):
		self.calculate_total_cost()
		for equipment in self.renting_details:
			self.check_availibility(equipment.equipment_name,equipment.from_date,equipment.to_date)


			if equipment.renting_type == "Daily":
				if not equipment.daily_rate:
					frappe.throw("Please Enter Daily Rate in <b>{0} Row</b>".format(equipment.idx))
			if equipment.renting_type == "Hourly":
				if not equipment.actual_hours:
					frappe.throw("Please Enter Rented Hours in <b>{0} Row</b>".format(equipment.idx))
				if not equipment.hourly_rate:
					frappe.throw("Please Enter Hourly Rate in <b>{0} Row</b>".format(equipment.idx))



	def calculate_total_cost(self):
		total_cost = 0
		for equipment in self.renting_details:
			if equipment.total_amount:
				total_cost = total_cost + float(equipment.total_amount)
		self.total_cost = total_cost
		frappe.db.set_value("Equipment Renting Timesheet",self.name,"total_cost",total_cost)


		# days = date_diff(self.to_date, self.from_date)
		# total_cost = (days+1)*self.daily_rate
		# self.total_cost = total_cost
		# self.rented_day = int(days+1)


	def update_equipment(self):
		frappe.db.set_value("Item",self.equipment_name,"last_booking_date",self.to_date)
		frappe.db.set_value("Item",self.equipment_name,"last_booking_time",self.to_time)
		frappe.db.set_value("Item",self.equipment_name,"equipment_status","Equiped")

	def make_gl_entry(self):
		pass

	def check_availibility(self,equipment,from_date,to_date):
		if getdate(from_date) > getdate(to_date):
			frappe.throw("To date cannot be less than From Date")
		if frappe.db.get_value("Equipment Booking",{"equipment":equipment},"name"):
			bookings = frappe.get_all("Equipment Booking",filters=[["from_date", ">=", frappe.utils.nowdate()], ["equipment","=", equipment]],fields=["from_date", "to_date"])
			for book in bookings:
				if getdate(from_date)<=getdate(book.from_date)<=getdate(to_date):
					frappe.throw("Choosen Date for Equipment <b>{equipment}</b> is <b>already booked</b> for {from_date} to {to_date}".format(from_date = book.from_date,to_date = book.to_date,equipment = equipment) )
				elif getdate(from_date)<=getdate(book.to_date)<=getdate(to_date):
					frappe.throw("Choosen Date for Equipment <b>{equipment}</b> is <b>already booked</b> for {from_date} to {to_date}".format(from_date = book.from_date,to_date = book.to_date,equipment = equipment) )
				elif getdate(book.from_date) <= getdate(from_date) and getdate(book.to_date) >= getdate(to_date):
					frappe.throw("Choosen Date for Equipment <b>{equipment}</b> is <b>already booked</b> for {from_date} to {to_date}".format(from_date = book.from_date,to_date = book.to_date,equipment = equipment) )


					

	def create_booking(self,equipment,from_date,to_date):
		object = frappe.get_doc({
		"doctype":"Equipment Booking",
		"equipment":equipment,
		"from_date":from_date,
		"to_date":to_date,
		"party":self.party,
		"timesheet":self.name
		})
		object.flags.ignore_permissions = True
		object.insert()


@frappe.whitelist()
def get_partners(item):
	partners = frappe.db.get_values("Equipment Partners",
        fieldname=["partner_name", "partner_distribution"],
        filters={
            "parent": item
        },
        as_dict=True
    )
	return partners

@frappe.whitelist()
def get_map_doc(source_name, target_doc=None):
	# frappe.throw(str(source_name))
	from frappe.model.mapper import get_mapped_doc
	def set_missing_values(source, target):
		pass
	def update_sales_invoice(obj, target, source_parent):
		target.ignore_pricing_rule = 1
    
	def update_item(obj, target, source_parent):
		# target.item_code = source_parent.equipment_name
		target.item_name = frappe.db.get_value("Item",obj.equipment_name,"item_name")
		target.uom = frappe.db.get_value("Item",obj.equipment_name,"stock_uom")
		target.description = frappe.db.get_value("Item",obj.equipment_name,"description")
		target.income_account = frappe.db.get_value("Company",source_parent.company,"default_income_account")
		target.cost_center = frappe.db.get_value("Company",source_parent.company,"cost_center")
		target.qty = 1
		# target.description = frappe.db.get_value("Item",source_parent.equipment_name,"description")
		

            
	doc = get_mapped_doc("Equipment Renting Timesheet", source_name,   {
		"Equipment Renting Timesheet": {
			"doctype": "Sales Invoice",
			"field_map": {
				"party":"customer",
				"doctype":"renting_type",
				"name":"rent_reference",
				"sales_partner_account":"sales_partner_account"
			},
			"postprocess": update_sales_invoice
		},
		"Multiple Equipment Rent": {
			"doctype": "Sales Invoice Item",
			"field_map": {
				"equipment_name":"item_code",
				"partner_group":"sales_partner",
				"partner_account":"sales_partner_account",
				"from_date":"from_date",
				"to_date":"to_date",
				"actual_hours":"rented_hours",
				"total_amount":"renting_rate"
				# "total_amount":"amount"
				# "total_amount":"price_list_rate"		
			},
			"postprocess": update_item
		},
		"Sales Team": {
			"doctype": "Sales Team",
			"field_map": {
								
			}
		}
		
	}, target_doc, set_missing_values)
 
	if frappe.db.get_value("Equipment Renting Timesheet",str(source_name),"timesheet_invoice"):
		frappe.throw("Invoice for this Timesheet is already created.")
	
	# doc.sales_team = [{
	# 	"sales_person":frappe.db.get_value("Equipment Renting Timesheet",str(source_name),"sales_person")
	# }]

	return doc

	





