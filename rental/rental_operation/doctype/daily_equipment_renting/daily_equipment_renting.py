# -*- coding: utf-8 -*-
# Copyright (c) 2019, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import date_diff,getdate

class DailyEquipmentRenting(Document):
	def on_update_after_submit(self):
		for details in self.equipment_renting_details:
			if (details.actual_hours and details.hourly_rate and not details.reason_to_change_hours):
					frappe.throw("Please Mention <b>Reason For Changing Hours</b> in <b>Row {}</b>".format(details.idx))
		# frappe.throw("here")
		pass
	def on_submit(self):
		self.make_sales_invoices()

	def validate(self):
		self.check_duplication()
		self.check_availability()
		self.check_fields()

	def make_sales_invoices(self):

		unique_customer  = frappe.db.sql("""select DISTINCT party from `tabDaily Renting Details` where parent = %s group by party""",(self.name),as_dict=1)

		for cust in unique_customer:
			customer_item = frappe.db.sql("""select equipment,renting_cost,item_partner_group,partner_group_account,location,sales_person,handling_employee,actual_hours,renting_type,hourly_rate from `tabDaily Renting Details` where parent = %s and party = %s""",(self.name,cust.party),as_dict=1)
			invoice_item=[]
			for item in customer_item:
				invoice_item.append({
					"item_code":item.equipment,
					"qty":1,
					"rented_day":1,
					"renting_type":item.renting_type,
					"rate":item.renting_cost,
					"daily_rate":item.renting_cost,
					"sales_partner":item.item_partner_group,
					"sales_partner_account":item.partner_group_account,
					"location":item.location,
					"handling_employee":item.handling_employee,
					"rented_hours":item.actual_hours,
					"hourly_rate":item.hourly_rate
				})
			# frappe.throw(str(invoice_item))

			sales_invoices = frappe.get_doc({
                "doctype":"Sales Invoice",
                "customer":cust.party,
				"renting_type":self.doctype,
				"rent_reference":self.name,
				"daily_equipment_renting":self.name,
				"ignore_pricing_rule":1,
				"due_date":self.date,
				"items":invoice_item,
				# "get_renting_price":1,
				"sales_team":[{
					"sales_person":customer_item[0]["sales_person"],
					"allocated_percentage":100
				}]
            })
			sales_invoices.flags.ignore_permissions = True
			sales_invoices.insert()
			# frappe.msgprint(str(customer_item))

		# frappe.throw("stop")


		# for item in self.equipment_renting_details:
			# sales_invoices = frappe.get_doc({
            #     "doctype":"Sales Invoice",
            #     "customer":item.party,
			# 	"renting_type":self.doctype,
			# 	"rent_reference":self.name,
			# 	"daily_equipment_renting":self.name,
			# 	"sales_partner_account":item.partner_group_account,
			# 	"due_date":self.date,
			# 	"items":[{
			# 		"item_code":item.equipment,
			# 		"qty":1,
			# 		"rented_day":1,
			# 		"rate":item.renting_cost
			# 	}]
            # })
			# sales_invoices.flags.ignore_permissions = True
			# sales_invoices.insert()


	def check_availability(self):
		equipmnent_data = frappe.db.sql("""select eb.equipment from `tabEquipment Booking` as eb where eb.from_date <= %s  and eb.to_date >= %s """,(self.date,self.date),as_dict = True)
		booked_equipment_list = [d['equipment'] for d in equipmnent_data if 'equipment' in d]

		for eq in self.equipment_renting_details:
			if eq.equipment in booked_equipment_list:
				frappe.throw("Equipment <b>{}</b> in row <b>{}</b> is already booked for Today".format(eq.equipment,eq.idx))

	def check_duplication(self):
		handling_employee = []
		for item in self.equipment_renting_details:
			if item.handling_employee not in handling_employee:
				handling_employee.append(item.handling_employee)
			else:
				frappe.throw("Handling Employee Can not be repeated!")
		equipment = []
		for item in self.equipment_renting_details:
			if item.equipment not in equipment:
				equipment.append(item.equipment)
			else:
				frappe.throw("Equipment Can not be repeated!")

	def check_fields(self):
		for eq in self.equipment_renting_details:
			if eq.renting_type == "Hourly":
				if not eq.actual_hours:
					frappe.throw("Please Enter Rented Hours in <b>{0} Row</b>".format(eq.idx))
				if not eq.hourly_rate:
					frappe.throw("Please Enter Hourly Rate in <b>{0} Row</b>".format(eq.idx))
		

			


# @frappe.whitelist()
# def check_availability(doctype,txt,searchfield,start,page_len,filters):
# 	# frappe.msgprint(str(filters.get('date')))
# 	equipmnent_data = frappe.db.sql("""select eb.equipment from `tabEquipment Booking` as eb where eb.from_date <= %s  and eb.to_date >= %s """,(filters.get('date'),filters.get('date')),as_dict = True)
# 	all_equipment =  frappe.get_all("Item",
#         fields=["name"],
#         filters= [["is_rental_equipment","=", 1 ]])

# 	booked_equipment_list = [d['equipment'] for d in equipmnent_data if 'equipment' in d] 

# 	# all_equipment_list = [d['name'] for d in all_equipment if 'name' in d]
	
# 	result_list = []
# 	# for l in all_equipment_list:
# 	# 	if l not in booked_equipment_list:
# 	# 		result_list.append(tuple(str(l)))
	

# 	for eq in all_equipment:
# 		if eq.name not in booked_equipment_list:
# 			result_list.append(eq.name)
# 	# frappe.throw(str(result_list))

# 	return result_list


