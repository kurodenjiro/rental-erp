// Copyright (c) 2019, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Daily Equipment Renting', {
	// refresh: function(frm) {

	// }
});

frappe.ui.form.on('Daily Renting Details', {

	
	actual_hours:function(frm,cdt,cdn){
		console.log("here")
		var df = frappe.meta.get_docfield("Daily Renting Details","reason_to_change_hours", frm.doc.name);
		console.log(df)
		var d = locals[cdt][cdn]
		// if(d.actual_hours){
		// 	df.reqd = 1
		// 	frm.refresh_field("equipment_renting_details")
        // }else{
		// 	df.reqd = 0
		// 	frm.refresh_field("equipment_renting_details")
		// }

		d.renting_cost = d.actual_hours * d.hourly_rate
		frm.refresh_field("equipment_renting_details")

	},
	hourly_rate:function(frm,cdt,cdn){
		var d = locals[cdt][cdn]
		cur_frm.script_manager.trigger("actual_hours", d.doctype, d.name);
	},
	equipment:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		frappe.model.get_value('Item Price', {'item_code': row.equipment,'price_list':'Standard Selling','selling':1}, 'price_list_rate',
			function(d) {
				// console.log(d)
				if(d){
					row.renting_cost = d.price_list_rate
					frm.refresh_field("equipment_renting_details")
				}
				
			})
	}

});


cur_frm.fields_dict['equipment_renting_details'].grid.get_field('equipment').get_query = function(doc) {
	return {
		filters:{ 
			"is_rental_equipment":1 
		}
	}
}

cur_frm.fields_dict['equipment_renting_details'].grid.get_field('handling_employee').get_query = function(doc) {
	return {
		filters:{ 
			"is_equipment_handler":1 
		}
	}
}