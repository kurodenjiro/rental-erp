from frappe import _

def get_data():
	return {
		'fieldname': 'daily_equipment_renting',
		'non_standard_fieldnames': {			
		},
		'internal_links': {			
		},
		'transactions': [			
			{
				'label': _('Reference'),
				'items': ["Sales Invoice"]
			}		
		]
	}