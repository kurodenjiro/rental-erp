// Copyright (c) 2019, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Equipment Maintenance', {
	onload:function(frm){
		frappe.model.get_value('Company', {'name': frm.doc.company}, 'cost_center', function(d){
			// console.log(d)
			frm.set_value("cost_center",d.cost_center)
			frm.refresh_field("cost_center")
		})
		
	},
	refresh: function(frm) {
		if(frm.doc.docstatus == 1 || frm.doc.docstatus == 2){

			cur_frm.add_custom_button(__('Accounts Ledger'), function() {
			frappe.route_options = {
			"voucher_no": cur_frm.doc.name,
			"from_date": cur_frm.doc.date,
			"to_date": cur_frm.doc.date,
			"company": cur_frm.doc.company,
			group_by_voucher: 0,
			group_by_account: 0,
			};
			frappe.set_route("query-report", "General Ledger");
			}, "View");


			cur_frm.add_custom_button(__('Stock Ledger'), function() {
			frappe.route_options = {
			"voucher_no": cur_frm.doc.name,
			"from_date": cur_frm.doc.date,
			"to_date": cur_frm.doc.date,
			"company": cur_frm.doc.company,
			group_by_voucher: 0,
			group_by_account: 0,
			};
			frappe.set_route("query-report", "Stock Ledger");
			}, "View");
		}


	},
	setup:function(frm){
		cur_frm.set_query("equipment",function(){
			return{
				filters:{
					"is_rental_equipment":1
				}
			}
		}),
		cur_frm.set_query("expense_account",function(){
			return{
				filters:{
					"parent_account":"Equipment Cost - AM",
					"account_type":"Expense Account"
				}
			}
		}),
		cur_frm.set_query("cost_center",function(){
			return{
				filters:{
					"is_group":0
				}
			}
		})
	},
	equipment:function(frm){
		if(frm.doc.equipment){
			frm.set_value("partners",[])
			frm.set_value("parts",[])
			frappe.call({
				method: "rental.rental_operation.doctype.equipment_renting_timesheet.equipment_renting_timesheet.get_partners",
				args: {"item":frm.doc.equipment},
				callback: function(r) {
					
					for (let i = 0; i < r.message.length; i++) {
						const element = r.message[i];
						var newrow = frappe.model.add_child(frm.doc, "Equipment Partners", "partners");
						newrow.partner_name = element['partner_name'];
						newrow.partner_distribution = element['partner_distribution'];
						refresh_field("partners");
						cur_frm.script_manager.trigger("partners", newrow.doctype, newrow.name);
					}
					frm.set_df_property("partners","read_only",1);
				}
			});
		}else{
			frm.set_value("equipment_name","")
			frm.set_value("partners",[])
			refresh_field("partners","equipment_name");
		}
	}
});

frappe.ui.form.on('Parts Details', {
	part:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		if(row.part && frm.doc.sales_partner_group){
			frappe.call({
				method:"rental.rental_operation.doctype.equipment_maintenance.equipment_maintenance.get_latest_valuation",
				args:{
					sales_owner:frm.doc.sales_partner_group,
					item:row.part
				},
				callback:function(res){

					console.log(res.message)
					row.rate = parseFloat(res.message)
					row.quantity = 1
					row.total_amount = parseFloat(res.message) * 1
					frm.refresh_field("parts")
				}
	
			})

		}
		
		// frappe.model.get_value('Item Price', {'item_code': row.part,'price_list':'Standard Selling','selling':1}, 'price_list_rate',
		// 	function(d) {
		// 		row.rate = d.price_list_rate
		// 		row.quantity = 1
		// 		row.total_amount = d.price_list_rate * 1
		// 		frm.refresh_field("parts")
		// 	})
		if(!frm.doc.sales_partner_group){
			frappe.throw(__("Please Select Equipment first"))
		}else{
			frappe.model.get_value('Sales Partner Group',frm.doc.sales_partner_group, 'repairing_item_warehouse', function(d){
				// console.log(d)
				row.warehouse = d.repairing_item_warehouse
				frm.refresh_field("parts")
			})
		}
		
	},
	quantity:function(frm,cdt,cdn){
		
		var row = locals[cdt][cdn]
		if(row.rate && row.quantity){
			row.total_amount = row.quantity * row.rate
			frm.refresh_field("parts")
		}
		
	},
	rate:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		cur_frm.script_manager.trigger("quantity", row.doctype, row.name);

	}
	
})


frappe.ui.form.on('Technician Details', {
	from_date:function(frm,cdt,cdn){
		var d = locals[cdt][cdn]
		if(d.from_date && d.to_date){
			var worked_days = frappe.datetime.get_day_diff(d.to_date,d.from_date )
			if(worked_days >= 0){
				d.worked_day = worked_days + 1
				// console.log(d.worked_day)
				// console.log(d.per_day_wages)
				d.amount = d.worked_day * d.per_day_wages
				frm.refresh_field("technician_details")
				// console.log(worked_days)
			}
			else{
				d.worked_day = ""
				d.amount = ""
				frm.refresh_field("technician_details")
				frappe.throw("<b>From Date</b> Should be Greater Than <b>to Date</b>")
			}
		}
		
	},
	to_date:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		cur_frm.script_manager.trigger("from_date", row.doctype, row.name);
	},
	per_day_wages:function(frm,cdt,cdn){
		var d = locals[cdt][cdn]
		if(d.worked_day){
			d.amount = d.worked_day * d.per_day_wages
			frm.refresh_field("technician_details")
		}
		
	}
})



cur_frm.fields_dict['parts'].grid.get_field('part').get_query = function(doc) {
	return {
		filters:{ 
			"is_repairing_item":1 
		}
	}
}


cur_frm.fields_dict['technician_details'].grid.get_field('technician').get_query = function(doc) {
	return {
		filters:{ 
			"is_equipment_technician":1 
		}
	}
}
