# -*- coding: utf-8 -*-
# Copyright (c) 2019, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from erpnext.controllers.accounts_controller import AccountsController
from erpnext.controllers.stock_controller import StockController
from erpnext.accounts.utils import get_fiscal_year, validate_fiscal_year, get_account_currency

class EquipmentMaintenance(StockController,AccountsController,Document):
	def validate(self):
		self.count_part_cost()
		self.count_technician_cost()
		self.count_total()

	def on_submit(self):
		self.deduct_stock_from_warehouse()
		# self.make_gl_entries_custom()

	def on_cancel(self):
		# frappe.throw("here")
		self.deduct_stock_from_warehouse()
		# self.make_gl_entries_custom()


	def count_part_cost(self):
		total = 0
		for item in self.parts:
			total = total + item.total_amount
		self.part_cost = total

	def count_technician_cost(self):
		# frappe.msgprint("here")
		total = 0
		for item in self.technician_details:
			total = float(total) + float(item.amount)
		# frappe.msgprint(str(total))
		self.total_technician_cost = total

	def count_total(self):
		# frappe.msgprint("total")
		self.total_cost = self.part_cost + self.total_technician_cost
		


	def make_gl_entries_custom(self,gl_entries=None):
		if not gl_entries:
			gl_entries = self.get_gl_entries()

		# frappe.throw(cstr(gl_entries))
		if gl_entries:
			from erpnext.accounts.general_ledger import make_gl_entries
			make_gl_entries(gl_entries, cancel=(self.docstatus == 2),
							update_outstanding="No", merge_entries=False)


	def get_gl_entries(self):
		# frappe.throw("get gl entry")
		gl_entries = []
		payment_account_currency = get_account_currency(self.expense_account)
		stock_account = frappe.db.get_value("Company",self.company,"default_inventory_account")
		# frappe.throw(str(get_partners(doc)))
		# for item in self.partners:
		# share = float((item.partner_distribution * self.part_cost)/100)
		if self.part_cost > 0:
			account_currency = get_account_currency(self.expense_account)

			gl_entries.append(
				self.get_gl_dict({
					"posting_date": self.date,
					"account": stock_account,
					"party_type":"Sales Partner Group",
					"party":self.sales_partner_group,
					"against": self.expense_account,
					"credit": float(self.part_cost),
					"credit_in_account_currency": float(self.part_cost),
					"remarks": "Againts Equipment Maintenance {}".format(self.name)
				}, payment_account_currency)
			)

			gl_entries.append(
				self.get_gl_dict({
					"posting_date": self.date,
					"account": self.expense_account,
					"against": stock_account,
					"debit": float(self.part_cost),
					"debit_in_account_currency": float(self.part_cost),
					"against_voucher": self.name,
					"against_voucher_type": self.doctype,
					"cost_center": self.cost_center,
					"remarks": "Againts Equipment Maintenance {}".format(self.name)
				}, account_currency)
			)

		return gl_entries

	def deduct_stock_from_warehouse(self):
		for d in self.parts:		
			sl_entries = []
			sl_entries.append(frappe._dict({
			"item_code": d.part,
			"warehouse": d.warehouse,
			"posting_date": self.date,				
			"voucher_type": self.doctype,
			"voucher_no": self.name,
			"actual_qty": (self.docstatus==1 and -1 or 1)*float(d.quantity),				
			"company": self.company,				
			"is_cancelled": self.docstatus==2 and "Yes" or "No"
			}))
			# frappe.throw(str(sl_entries))
			self.make_sl_entries(sl_entries, self.docstatus==2 and 'Yes' or 'No')	

@frappe.whitelist()
def get_latest_valuation(sales_owner,item):
	warehouse = frappe.db.get_value("Sales Partner Group",sales_owner,"repairing_item_warehouse")
	data = frappe.db.sql("""select valuation_rate from `tabStock Ledger Entry` where warehouse = %s and item_code =  %s order by posting_date desc,posting_time desc""",(warehouse,item),as_dict = True)
	if data:
		return data[0].valuation_rate
	else:
		return 0.00

	
			

