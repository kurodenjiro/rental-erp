# -*- coding: utf-8 -*-
# Copyright (c) 2019, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class SalesPartnerGroup(Document):
	def validate(self):
		pass
		
		
	
	def after_insert(self):
		if self.financial_partner == 1:
			# frappe.msgprint("here")
			object = frappe.get_doc({
			"doctype":"Account",
			"account_name":self.name,
			"parent_account":"Sales Partner - "+str(frappe.db.get_value("Company",str(frappe.db.get_value("Global Defaults",None,"default_company")),"abbr")),
			# change
			"account_type":"Payable",
			"company":str(frappe.db.get_value("Global Defaults",None,"default_company"))
			})
			object.flags.ignore_permissions = True
			object.insert()
			frappe.db.set_value("Sales Partner Group",self.name,"sales_group_account",object.name)

			object = frappe.get_doc({
			"doctype":"Account",
			"account_name":"Cash " + str(self.name),
			"parent_account":"Cash In Hand - "+str(frappe.db.get_value("Company",str(frappe.db.get_value("Global Defaults",None,"default_company")),"abbr")),
			# change
			"account_type":"Cash",
			"company":str(frappe.db.get_value("Global Defaults",None,"default_company"))
			})
			object.flags.ignore_permissions = True
			object.insert()
			# frappe.throw(str(object.name))
			self.cash_account = str(object.name)
			frappe.db.set_value("Sales Partner Group",self.name,"cash_account",str(object.name))

			object = frappe.get_doc({
			"doctype":"Warehouse",
			"warehouse_name":"Store "+self.name,
			"parent_warehouse":"All Warehouses - "+str(frappe.db.get_value("Company",str(frappe.db.get_value("Global Defaults",None,"default_company")),"abbr")),
			"company":str(frappe.db.get_value("Global Defaults",None,"default_company"))
			})
			object.flags.ignore_permissions = True
			object.insert()
			self.repairing_item_warehouse = str(object.name)
			frappe.db.set_value("Sales Partner Group",self.name,"repairing_item_warehouse",object.name)
			self.update_sales_partner()
			

	def update_sales_partner(self):
		for partner in self.sales_partners:
			frappe.db.set_value("Sales Partner",partner.partner_name,"sales_partner_group",self.name)
			# frappe.msgprint(str(partner.partner_name))

def test():
	pass