# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	columns = [
		# _("SR No.") + ":Int:50", 
		_("Invoice Date") + ":Date:90",
		_("Supplier") + ":Link/Supplier:100",	
		_("Invoice") + ":Link/Purchase Invoice:140",	
		_("Invoice Type") + ":Data:120",
		_("Purchase Invoice Amount") + ":Float:180",
		_("Paid Amount") + ":Float:180",	
		_("Balance") + ":Float:180"	
	]

	return columns

# def get_data(filters):
# 	data = []
	
# 	purchase_invoice = frappe.db.sql("""select p.supplier,sum(p.grand_total) as grand_total from (Select pi.grand_total ,pi.supplier from `tabPurchase Invoice` as pi where pi.is_equipment_invoice = 1 and pi.partner_group_account_ = %s and pi.posting_date >= %s and pi.posting_date <= %s and pi.docstatus = 1 UNION
# 	Select pi.grand_total,pi.supplier from `tabPurchase Invoice` as pi where pi.sales_partner_group = %s and pi.posting_date > %s and pi.posting_date < %s and pi.docstatus = 1) as p group by p.supplier""",(frappe.db.get_value("Sales Partner Group",filters.get("sales_owner"),"cash_account"),filters.get("from_date"),filters.get("to_date"),filters.get("sales_owner"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
# 	for inv in purchase_invoice:
# 		paid_amount = 0
# 		payment = frappe.db.sql("""SELECT pe.name,sum(pe.paid_amount) as recieved_amount FROM `tabPayment Entry` as pe where pe.party = %s and pe.docstatus = 1 and pe.payment_type = "Pay" and pe.paid_from = %s and pe.posting_date >= %s and pe.posting_date <= %s group by pe.party""",(inv.supplier,frappe.db.get_value("Sales Partner Group",filters.get("sales_owner"),"cash_account"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
# 		if len(payment) > 0:
# 			paid_amount = float(payment[0].recieved_amount)	
# 		data.append([inv.supplier,float(inv.grand_total),paid_amount,float(inv.grand_total)-paid_amount])
# 	# frappe.throw("stop")
# 	return data


def get_data(filters):
	data = []
	equipment_purchase_invoice = frappe.db.sql("""SELECT p.posting_date,pi.sales_owner,sum(pi.amount) as total_purchase,p.name,p.supplier,p.total_taxes_and_charges,p.total FROM `tabPurchase Invoice Item` AS pi INNER JOIN `tabPurchase Invoice` AS p ON pi.parent = p.name where p.docstatus = 1 and p.is_equipment_invoice = 1 and pi.sales_owner = %s and p.posting_date BETWEEN %s AND %s group by pi.sales_owner""",(filters.get("sales_owner"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	
	for inv in equipment_purchase_invoice:
		
		paid_amount = 0
		payment = frappe.db.sql("""SELECT ji.account,sum(ji.credit_in_account_currency) as total_paid from `tabJournal Entry Account` as ji inner join `tabJournal Entry` as je on ji.parent = je.name where je.docstatus = 1 and je.remark = %s and ji.account = %s and je.posting_date BETWEEN %s AND %s group by je.remark""",(inv.name,frappe.db.get_value("Sales Partner Group",inv.sales_owner,"cash_account"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
		# frappe.throw(str(payment))
		if len(payment) > 0:
			paid_amount = float(payment[0].total_paid)
		if inv.total_taxes_and_charges:
			total_purchase_amount = float(inv.total_purchase) + float((inv.total_taxes_and_charges * inv.total_purchase)/inv.total)
		else:
			total_purchase_amount = inv.total_purchase	
		data.append([inv.posting_date,inv.supplier,inv.name,"Equipment Expense",total_purchase_amount,paid_amount,total_purchase_amount-paid_amount])

	stock_purchase_invoice = frappe.db.sql("""SELECT p.posting_date,p.supplier,p.sales_partner_group,p.grand_total as total_purchase,p.name from `tabPurchase Invoice` as p where p.docstatus = 1 and p.is_equipment_invoice = 0 and p.posting_date BETWEEN %s AND %s""",(filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	# frappe.throw(str(stock_purchase_invoice))
	for inv in stock_purchase_invoice:
		
		paid_amount = 0
		payment = frappe.db.sql("""SELECT ji.account,sum(ji.credit_in_account_currency) as total_paid from `tabJournal Entry Account` as ji inner join `tabJournal Entry` as je on ji.parent = je.name where je.docstatus = 1 and je.remark = %s and ji.account = %s and je.posting_date BETWEEN %s AND %s group by je.remark""",(inv.name,frappe.db.get_value("Sales Partner Group",inv.sales_partner_group,"cash_account"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
		# frappe.throw(str(payment))
		if len(payment) > 0:
			paid_amount = float(payment[0].total_paid)	
		data.append([inv.posting_date,inv.supplier,inv.name,"Stock Expense",float(inv.total_purchase),paid_amount,float(inv.total_purchase)-paid_amount])
	
	# frappe.throw("stop")
	return data

