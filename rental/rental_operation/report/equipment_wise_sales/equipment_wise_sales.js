// Copyright (c) 2016, Raj Tailor and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Equipment Wise Sales"] = {
	"filters": [
		{
			"fieldname":"equipment",
			"label": __("Equipment name"),
			"fieldtype": "Link",
			"options": "Item",
			"reqd":1,
			"filters":{
				"is_rental_equipment" : 1
			}
		},
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today()
		}

	]
};
