# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	columns = [
		# _("SR No.") + ":Int:50", 
		# _("Customer") + ":Link/Customer:160",
		_("Invoice Date") + ":Date:100",
		_("Sales Invoice") + ":Link/Sales Invoice:160",
		_("Sales Invoice Amount") + ":Float:200",		
		_("Payment Date") + ":Date:100",
		_("Recieved Amount") + ":Float:200",
		_("Outstanding Amount") + ":Data:200",	
	]

	return columns

def get_data(filters):
	data = []
	
	sales_invoice = frappe.db.sql("""SELECT si.customer,si.posting_date,si.name,si.grand_total from `tabSales Invoice` AS si WHERE si.docstatus = 1 and si.customer = %s and si.posting_date >= %s and si.posting_date <= %s""",(filters.get("customer"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	

	# frappe.throw(str(sales_invoice))
	for invoice in sales_invoice:
		paid_amount = 0
		voucher = ""
		if invoice:
			payment = frappe.db.sql("""SELECT pe.posting_date,sum(pe.total_allocated_amount) as paid_amount FROM `tabPayment Entry Reference` as pef INNER JOIN `tabPayment Entry` as pe ON pef.parent = pe.name where pef.reference_doctype = "Sales Invoice" and pef.reference_name = %s and pe.docstatus = 1 and pe.posting_date >= %s and pe.posting_date <= %s group by pe.posting_date""",(invoice.name,filters.get("from_date"),filters.get("to_date")),as_dict = 1)
			frappe.msgprint(str(invoice))
			frappe.msgprint(str(payment))
			
			if len(payment)>0:
				invoice_amount = invoice.grand_total
				paid = 0
				for pe in range(0,len(payment)):
					paid = paid + payment[pe].paid_amount
					if pe == 0:
						data.append([invoice.posting_date,invoice.name,invoice.grand_total,payment[pe].posting_date,payment[pe].paid_amount,str(invoice_amount-paid)])
					else:
						data.append(["","","",payment[pe].posting_date,payment[pe].paid_amount,0])
			else:
				data.append([invoice.posting_date,invoice.name,invoice.grand_total,"",0,str(invoice.grand_total)])
			
	return data

