# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	columns = [
		# _("SR No.") + ":Int:50", 
		_("Date") + ":Date:150",
		_("Sales Invoice") + ":Link/Sales Invoice:160",
		_("Customer") + ":Link/Customer:160",
		_("Sales Invoice Amount") + ":Float:200",
		_("Recieved Amount") + ":Float:200",	
		_("Balance") + ":Float:200"	
	]

	return columns

def get_data(filters):
	data = []
	
	sales_invoice = frappe.db.sql("""SELECT si.posting_date,si.name,sic.sales_partner_account,sic.sales_partner,si.customer,sum(sic.amount) as invoice_amount,si.total,si.total_taxes_and_charges from `tabSales Invoice Item` AS sic INNER JOIN `tabSales Invoice` AS si ON sic.parent = si.name WHERE si.docstatus = 1 and sic.sales_partner = %s and si.posting_date >= %s and si.posting_date <= %s group by si.name,sic.sales_partner""",(filters.get("sales_owner"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	for invoice in sales_invoice:
		paid_amount = 0
		if invoice:
			payment = frappe.db.sql("""SELECT pef.reference_name,pe.name,pe.equipment_name,sum(pe.paid_amount) as recieved_amount FROM `tabPayment Entry Reference` as pef INNER JOIN `tabPayment Entry` as pe ON pef.parent = pe.name where pef.reference_name = %s and pe.sales_partner_account = %s and pe.docstatus = 1 and pe.posting_date >= %s and pe.posting_date <= %s group by pef.reference_name,pe.sales_partner_account""",(invoice.name,invoice.sales_partner_account,filters.get("from_date"),filters.get("to_date")),as_dict = 1)
			if len(payment) > 0:
				paid_amount = float(payment[0].recieved_amount)
			if invoice.total_taxes_and_charges:
				total_sales_amount = float(invoice.invoice_amount) + float((invoice.total_taxes_and_charges * invoice.invoice_amount)/invoice.total)
			else:
				total_sales_amount = invoice.invoice_amount
			# frappe.msgprint(str(invoice))
			# frappe.msgprint(str(payment))
		data.append([invoice.posting_date,invoice.name,invoice.customer,total_sales_amount,paid_amount,total_sales_amount-paid_amount])
	return data
	# frappe.throw("stop")
