# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	columns = [ 
		_("Sales Owner") + ":Link/Sales Partner Group:100",
		_("Sales Invoice Amount") + ":Float:200",
		_("Recieved Amount") + ":Float:200",	
		_("Balance") + ":Float:200"
	]

	return columns

def get_data(filters):
	data = []
	get_supplier = frappe.db.get_values("Sales Partner Group",
        fieldname=["name"],
        filters={
            "financial_partner": 1
        },
        as_dict=True
    )
	for supp in get_supplier:
		sales_invoice_total  = 0.00
		paid_total  = 0.00
		sales_invoice = frappe.db.sql("""SELECT SUM(sic.amount + (si.total_taxes_and_charges * sic.amount)/si.total) as actual_amount from `tabSales Invoice Item` AS sic INNER JOIN `tabSales Invoice` AS si ON sic.parent = si.name WHERE sic.sales_partner = %s and si.docstatus = 1 and si.posting_date >= %s and si.posting_date <= %s group by sic.sales_partner""",(supp.name,filters.get("from_date"),filters.get("to_date")))
		if sales_invoice:
			# frappe.ms
			sales_invoice_total = float(sales_invoice[0][0])
		payment = frappe.db.sql("""SELECT sum(pe.paid_amount) as recieved_amount FROM `tabPayment Entry` as pe where pe.sales_partner_account = %s and pe.docstatus = 1 and pe.posting_date >= %s and pe.posting_date <= %s group by pe.sales_partner_account""",(frappe.db.get_value("Sales Partner Group",supp.name,"sales_group_account"),filters.get("from_date"),filters.get("to_date")))
		if payment:
			paid_total = float(payment[0][0])

		data.append([supp.name,float(sales_invoice_total),float(paid_total),float(float(sales_invoice_total)-float(paid_total))])

		
	return data

