# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	columns = [
		{
			"label": _("Posting Date"),
			"fieldname": "posting_date",
			"fieldtype": "Date",
			"width": 80
		},
		{
			"label": _("Voucher Type"),
			"fieldname": "voucher_type",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label": _("Sales Invoice Amount"),
			"fieldname": "total_amount",
			"fieldtype": "Currency",
			"width": 200
		},	
		{
			"label": _("Received Amount"),
			"fieldname": "received_amount",
			"fieldtype": "Currency",
			"width": 130
		},
		{
			"label": _("Outstanding Amount"),
			"fieldname": "balance_amount",
			"fieldtype": "Currency",
			"width": 200
		}
			
	]

	return columns

def get_data(filters):
	data = []
	
	sales_invoice = frappe.db.sql("""SELECT si.customer,si.posting_date,si.name,si.grand_total from `tabSales Invoice` AS si WHERE si.docstatus = 1 and si.customer = %s and si.posting_date >= %s and si.posting_date <= %s""",(filters.get("customer"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)

	payment = frappe.db.sql("""SELECT pe.party,sum(total_allocated_amount) as paid_amount,pe.posting_date FROM `tabPayment Entry Reference` as pef INNER JOIN `tabPayment Entry` as pe ON pef.parent = pe.name where pef.reference_doctype = "Sales Invoice" and pe.party = %s and pe.docstatus = 1 and pe.posting_date >= %s and pe.posting_date <= %s group by pe.posting_date""",(filters.get("customer"),filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	

	all_record = sorted(sales_invoice + payment, key = lambda i: i['posting_date'])
	grand_total = 0.00 
	paid_amount = 0.00
	out_standing = 0.00
	for element in all_record:
		if 'name' in element.keys():
			# frappe.msgprint(str(element))
			grand_total = grand_total + element['grand_total']
			out_standing = out_standing + element['grand_total']
			data.append([element['posting_date'],element['name'],element['grand_total'],0,out_standing])
		else:
			paid_amount = paid_amount + element['paid_amount']
			out_standing = out_standing - element['paid_amount']
			data.append([element['posting_date'],"Payment Voucher",0,element['paid_amount'],out_standing])

			# frappe.msgprint(str(element))
	data.append(["","Total",grand_total,paid_amount,out_standing])

	# for element in all_record:


	

	# frappe.msgprint(str(all_record))
	


	# frappe.throw(str(sales_invoice))
	# for invoice in sales_invoice:
	# 	paid_amount = 0
	# 	voucher = ""
	# 	if invoice:
	# 		payment = frappe.db.sql("""SELECT pe.posting_date,sum(pe.total_allocated_amount) as paid_amount FROM `tabPayment Entry Reference` as pef INNER JOIN `tabPayment Entry` as pe ON pef.parent = pe.name where pef.reference_doctype = "Sales Invoice" and pef.reference_name = %s and pe.docstatus = 1 and pe.posting_date >= %s and pe.posting_date <= %s group by pe.posting_date""",(invoice.name,filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	# 		# frappe.msgprint(str(invoice))
	# 		# frappe.msgprint(str(payment))
			
	# 		if len(payment)>0:
	# 			invoice_amount = invoice.grand_total
	# 			paid = 0
	# 			for pe in range(0,len(payment)):
	# 				paid = paid + payment[pe].paid_amount
	# 				if pe == 0:
	# 					data.append([invoice.posting_date,invoice.name,invoice.grand_total,payment[pe].posting_date,payment[pe].paid_amount,str(invoice_amount-paid)])
	# 				else:
	# 					data.append(["","","",payment[pe].posting_date,payment[pe].paid_amount,0])
	# 		else:
	# 			data.append([invoice.posting_date,invoice.name,invoice.grand_total,"",0,str(invoice.grand_total)])
			
	return data

