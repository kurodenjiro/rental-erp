// Copyright (c) 2016, Raj Tailor and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Sales Owner Wise Earning and Expense"] = {
	"filters": [
		// {
		// 	"fieldname":"sales_owner",
		// 	"label": __("Sales Owner"),
		// 	"fieldtype": "Link",
		// 	"options": "Sales Partner Group",
		// 	"reqd":1
		// },
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today()
		}
	]
};
