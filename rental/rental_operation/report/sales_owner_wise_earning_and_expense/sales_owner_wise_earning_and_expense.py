# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from erpnext.accounts.utils import get_balance_on

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data
	


def get_columns(filters):
	columns = [
		# _("SR No.") + ":Int:50", 
		_("Sales Owner") + ":Link/Sales Partner Group:100",
		_("Sales Earnings") + ":Float:100",
		_("Expense From Maintenance") + ":Float:200",
		_("Expense From Purchase") + ":Float:200",
		_("Expense From Expense Account") + ":Float:200",
		_("Net Earnings") + ":Float:100"
	]

	return columns

def get_data(filters):
	# frappe.msgprint(str(filters.get("from_date")))
	# frappe.msgprint(str(filters.get("to_date")))
	data = []
	
	get_supplier = frappe.db.get_values("Sales Partner Group",
        fieldname=["name"],
        filters={
            "financial_partner": 1
        },
        as_dict=True
    )
	for supp in get_supplier:
		sales_invoice_total  = 0.00
		maintenance_total  = 0.00
		purchase_total  = 0.00
		expense_from_account = 0.00
		sales_invoice = frappe.db.sql("""SELECT SUM(sic.amount + (si.total_taxes_and_charges * sic.amount)/si.total) as actual_amount from `tabSales Invoice Item` AS sic INNER JOIN `tabSales Invoice` AS si ON sic.parent = si.name WHERE sic.sales_partner = %s and si.docstatus = 1 and si.posting_date >= %s and si.posting_date <= %s group by sic.sales_partner""",(supp.name,filters.get("from_date"),filters.get("to_date")))
		if sales_invoice:
			sales_invoice_total = float(sales_invoice[0][0])


		maintenance = frappe.db.sql("""SELECT sum(em.total_cost) from `tabEquipment Maintenance` AS em where em.sales_partner_group = %s and em.date >= %s and em.date <= %s and em.docstatus = 1 group by em.sales_partner_group""",(supp.name,filters.get("from_date"),filters.get("to_date")))
		if maintenance:
			maintenance_total = float(maintenance[0][0])

		purchase = frappe.db.sql("""SELECT SUM(pi.amount + (p.total_taxes_and_charges * pi.amount)/p.total) as actual_amount FROM `tabPurchase Invoice Item` AS pi INNER JOIN `tabPurchase Invoice` AS p ON pi.parent = p.name where pi.sales_owner = %s and p.docstatus = 1 and p.is_equipment_invoice = 1 and p.posting_date BETWEEN %s AND %s group by pi.item_code""",(supp.name,filters.get("from_date"),filters.get("to_date")))

		
		if purchase:
			purchase_total = float(purchase[0][0])

		if frappe.db.get_value("Sales Partner Group",supp,"expense_account"):
			expense_from_account = get_balance_on(account=frappe.db.get_value("Sales Partner Group",supp,"expense_account"), date=filters.get("to_date"))
			

		data.append([supp.name,float(sales_invoice_total),float(maintenance_total),float(purchase_total),float(expense_from_account),float(float(sales_invoice_total)-float(maintenance_total)-float(purchase_total) - float(expense_from_account))])
	
	return data

