# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters),get_data(filters)
	return columns, data


def validate_filters(filters):
	if filters.from_date > filters.to_date:
		frappe.throw(_("From Date must be before To Date"))

def get_data(filters):
	data = []
	if filters.get("sales_owner"):
		get_item = frappe.db.get_values("Item",
			fieldname=["name","item_name","partner_group"],
			filters={
				"is_rental_equipment": 1,
				"partner_group":filters.get("sales_owner")
			},
			as_dict=True
		)
	else:
		get_item = frappe.db.get_values("Item",
			fieldname=["name","item_name","partner_group"],
			filters={
				"is_rental_equipment": 1
			},
			as_dict=True)

	for item in get_item:
		sales_invoice_total  = 0.00
		maintenance_total  = 0.00
		purchase_total  = 0.00
		sales_invoice = frappe.db.sql("""SELECT SUM(sic.amount + (si.total_taxes_and_charges * sic.amount)/si.total) as actual_amount from `tabSales Invoice Item` AS sic INNER JOIN `tabSales Invoice` AS si ON sic.parent = si.name WHERE sic.item_code = %s and si.docstatus = 1 and si.posting_date >= %s and si.posting_date <= %s group by sic.item_code""",(item.name,filters.get("from_date"),filters.get("to_date")))
		# frappe.msgprint(str(item))
		# frappe.msgprint(str(sales_invoice))
		
		if sales_invoice:
			sales_invoice_total = float(sales_invoice[0][0])


		maintenance = frappe.db.sql("""SELECT sum(em.total_cost) from `tabEquipment Maintenance` AS em where em.equipment = %s and em.date >= %s and em.date <= %s and em.docstatus = 1 group by em.equipment""",(item.name,filters.get("from_date"),filters.get("to_date")))
		if maintenance:
			maintenance_total = float(maintenance[0][0])

		# purchase = frappe.db.sql("""SELECT sum(pi.grand_total) from `tabPurchase Invoice` AS pi where pi.equipment = %s and pi.posting_date >= %s and pi.posting_date <= %s and pi.docstatus = 1 group by pi.equipment""",(item.name,filters.get("from_date"),filters.get("to_date")))
		# purchase = frappe.db.sql("""Select sum(pi.amount) from `tabPurchase Invoice Item` as pi inner join `tabPurchase Invoice` as p on pi.parent = p.name where p.docstatus = 1 and p.is_equipment_invoice = 1 and pi.item_code = %s and p.posting_date >= %s and p.posting_date <= %s group by pi.item_code""",(item.name,filters.get("from_date"),filters.get("to_date")))

		purchase = frappe.db.sql("""SELECT SUM(pi.amount + (p.total_taxes_and_charges * pi.amount)/p.total) as actual_amount FROM `tabPurchase Invoice Item` AS pi INNER JOIN `tabPurchase Invoice` AS p ON pi.parent = p.name where pi.item_code = %s and p.docstatus = 1 and p.is_equipment_invoice = 1 and p.posting_date BETWEEN %s AND %s group by pi.item_code""",(item.name,filters.get("from_date"),filters.get("to_date")))
		
		# frappe.throw(str(purchase))
		if purchase:
			purchase_total = float(purchase[0][0])

		data.append([item.name,item.item_name,item.partner_group,float(sales_invoice_total),float(maintenance_total),float(purchase_total),float(float(sales_invoice_total)-float(maintenance_total)-float(purchase_total))])
	
	return data

def get_columns(filters):
	columns = [
		_("Equipment Code") + ":Link/Item:150", 
		_("Equipment Name") + ":Data:150",		
		_("Sales Owner") + ":Link/Sales Partner Group:150",
		_("Sales Earnings") + ":Float:100",
		_("Expense From Maintenance") + ":Float:200",
		_("Expense From Purchase") + ":Float:200",
		_("Net Earnings") + ":Float:100"		
	]

	return columns